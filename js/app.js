var products = [
    {
        sku: '001',
        name:   'Cotton T-Shirt, Medium',
        price:  1.99,
        qty:    1
    },
    {
        sku: '002',
        name:   'Baseball Cap, One Size',
        price:  2.99,
        qty:    2
    },
    {
        sku: '003',
        name:   'Swim Shorts, Medium',
        price:  3.99,
        qty:    1
    }
];


$(document).ready(function() {
    $('#basket').basket(products);
})