(function ($) {
    var elem;
    // Plugin definition.
    $.fn.basket = function(products) {
        var products = products;
        elem = $(this);
        elem.addClass('basket-component');

        var table = $.fn.basket.renderTable(products);
        elem.append(table);

        var detailsPanel = $.fn.basket.renderDetails(products);
        elem.append(detailsPanel);
    };
     
    $.fn.basket.renderTable = function(products) {
        var table = document.createElement('table');
        var columns = [
            {
                label: 'Product',
                index: 'name',
                class: 'left'
            },
            {
                label: 'Price',
                index: 'price',
                class: 'right'
            },
            {
                label: 'Qty',
                index: 'qty',
                class: ''
            },
            {
                label: 'Cost',
                index: false,
                class: 'right'
            }
        ];
        var tableHead = document.createElement('thead');
        var tr =  document.createElement('tr');

        $.each(columns, function(index, column) {
            var th = document.createElement('th');
            th.className = column.class;
            th.appendChild(
                document.createTextNode(column.label)
            );

            tr.appendChild(th);
        });

        tableHead.appendChild(tr);
        table.appendChild(tableHead);

        var tbody = document.createElement('tbody');
        $.each(products, function(trIndex, product) {
            var tr = document.createElement('tr');

            $.each(columns, function(tdIndex, column) {
                var td = document.createElement('td');
                td.className = column.class;

                var fields = {
                    product: $.fn.basket.renderProductField,
                    price: $.fn.basket.renderPriceField,
                    qty: $.fn.basket.renderQtyField,
                    cost: $.fn.basket.renderCostField
                };

                var elements = fields[column.label.toLowerCase()](product, column, tr, tbody);

                $.each(elements, function(index, element){
                    td.appendChild(element);
                });

                tr.appendChild(td);
            });

            tbody.appendChild(tr);
        });

        table.appendChild(tbody);

        return table;
    };

    $.fn.basket.renderProductField = function(product, column, tr, tbody) {
        return [document.createTextNode(product[column.index])]
    };

    $.fn.basket.renderPriceField = function(product, column, tr, tbody) {
        return [document.createTextNode('£'+product[column.index])]
    };

    $.fn.basket.renderQtyField = function(product, column, tr, tbody) {
        var input = document.createElement('input');
        input.value = product.qty;
        input.addEventListener('keydown', function(e) {
            if(e.keyCode === 8 || e.keyCode === 9 || (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            } else if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            } else {
                var newValue = e.target.value.toString() + String.fromCharCode(e.keyCode);
                newValue = parseInt(newValue)
                if(newValue > 10 || newValue < 1){
                    e.preventDefault();
                }
            } 
        });
        input.addEventListener('keyup', function(e) {
            product.qty = input.value;
            $.fn.basket.changeQty(product, tr);
        });

        var qtyControls = document.createElement('div');
        qtyControls.className = 'qty-controls';

        var add = document.createElement('div');
        add.className = 'add';
        add.appendChild(
            document.createTextNode('+')
        );
        add.addEventListener('click', function(){
            $.fn.basket.addQty(product, input, tr);
        });
        qtyControls.appendChild(add);

        var substract = document.createElement('div');
        substract.className = 'substract';
        substract.appendChild(
            document.createTextNode('-')
        );
        substract.addEventListener('click', function(){
            $.fn.basket.substractQty(product, input, tr);
        });
        qtyControls.appendChild(substract);

        return [input, qtyControls];
    };

    $.fn.basket.renderCostField = function(product, column, tr, tbody) {
        product.cost = product.qty * product.price;

        var costWrap = document.createElement('span');
        costWrap.appendChild(
            document.createTextNode('£'+product.cost.toFixed(2))
        );

        var trashBin = document.createElement('div');
        trashBin.className = 'trash-bin';
        var icon = document.createElement('img');
        icon.src = 'img/trash-bin.png';
        icon.alt = 'Remove Item';

        trashBin.addEventListener('click', function(){
            var toRemove;
            $.each(tbody.childNodes, function(index, child){
                if(child === tr) {
                   toRemove = index;
                }
            });

            tbody.removeChild(tbody.childNodes[toRemove]);
            products.splice(toRemove, 1);

            if(products.length === 0){
                elem.find('.buy-now button').prop('disabled', true);
            }

            $.fn.basket.updateDetails(products);
        });

        trashBin.appendChild(icon);

        return [costWrap, trashBin];
    };

    $.fn.basket.changeQty = function(product, tr) {
        product.cost = product.qty * product.price;
        tr.childNodes[3].childNodes[0].textContent = '£'+product.cost.toFixed(2);

        $.fn.basket.updateDetails(products);
    }

    $.fn.basket.addQty = function(product, input, tr) {
        if(product.qty < 10){
            product.qty++;
            input.value = product.qty;
            $.fn.basket.changeQty(product, tr);
        }
    }

    $.fn.basket.substractQty = function(product, input, tr) {
        if(product.qty > 1){
            product.qty--;
            input.value = product.qty;
            $.fn.basket.changeQty(product, tr);
        }
    }

    $.fn.basket.renderDetails = function(products) {
        var columns = [
            {
                label: 'Subtotal',
                class: 'subtotal',
                method: $.fn.basket.getSubtotal
            },
            {
                label: 'VAT @ 20%',
                class: 'vat',
                method: $.fn.basket.getVat
            },
            {
                label: 'Total Cost',
                class: 'total',
                method: $.fn.basket.getTotal
            }
        ];
        var details = document.createElement('div');
        details.className = 'details';
        var label, amount;

        $.each(columns, function(index, column) {
            var columnWrap = document.createElement('div');
            columnWrap.className = column.class;
            label = document.createElement('div');
            label.appendChild(
                document.createTextNode(column.label)
            );
            columnWrap.appendChild(label);
            amount = document.createElement('div');
            amount.className = 'amount';
            amount.appendChild(
                document.createTextNode('£'+column.method(products))
            );
            columnWrap.appendChild(amount);
            details.appendChild(columnWrap);
        });

        var buyNow = document.createElement('div');
        buyNow.className = 'buy-now';
        var btn = document.createElement('button');
        btn.appendChild(
            document.createTextNode('Buy Now »')
        );
        btn.addEventListener('click', function(){
             $.fn.basket.sendData(products);
        });

        buyNow.appendChild(btn);
        details.appendChild(buyNow);


        return details;
    }

    $.fn.basket.getSubtotal = function(products) {
        var subtotal = 0;

        $.each(products, function(index, product) {
            subtotal += product.cost;
        });

        return subtotal.toFixed(2);
    };

    $.fn.basket.getVat = function(products) {
        var subtotal = $.fn.basket.getSubtotal(products);
        var vat = subtotal * 0.2;

        return vat.toFixed(2);
    };

    $.fn.basket.getTotal = function(products) {
        var subtotal = $.fn.basket.getSubtotal(products);
        var vat = $.fn.basket.getVat(products);
        var total = parseFloat(subtotal) + parseFloat(vat);

        return total.toFixed(2);
    };

    $.fn.basket.updateDetails = function(products) {
        var details = elem.find('.details');
        var subtotal = $.fn.basket.getSubtotal(products);
        var vat = $.fn.basket.getVat(products);
        var total = $.fn.basket.getTotal(products);      

        details.find('.subtotal .amount').text('£'+subtotal);
        details.find('.vat .amount').text('£'+vat);
        details.find('.total .amount').text('£'+total);
    };

    $.fn.basket.sendData = function(products) {
        var basket = {
            products: products,
            subtotal: $.fn.basket.getSubtotal(products),
            vat: $.fn.basket.getVat(products),
            total: $.fn.basket.getTotal(products)
        };

        $.ajax({
            type: 'POST',
            url: '',
            data: JSON.stringify(basket),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function(){
                //Nice!
            }
        });
    };
}(jQuery));