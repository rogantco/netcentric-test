var gulp    = require('gulp');
var watch   = require('gulp-watch');
var connect = require('gulp-connect');
var sass    = require('gulp-sass');

watch(['./*.html', './js/*.js'])
        .pipe(connect.reload());

gulp.task('webserver', function() {
    connect.server({
        livereload: true
    });
});

gulp.task('sass', function () {
  return gulp.src('./styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./styles'))
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch('./styles/*.scss', ['sass']);
});

 
gulp.task('default', ['webserver', 'sass', 'watch']);